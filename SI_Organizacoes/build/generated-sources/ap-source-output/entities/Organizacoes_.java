package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2016-03-28T00:45:55")
@StaticMetamodel(Organizacoes.class)
public class Organizacoes_ { 

    public static volatile SingularAttribute<Organizacoes, Integer> id;
    public static volatile SingularAttribute<Organizacoes, String> morada;
    public static volatile SingularAttribute<Organizacoes, String> email;
    public static volatile SingularAttribute<Organizacoes, String> telefone;
    public static volatile SingularAttribute<Organizacoes, String> designacao;

}