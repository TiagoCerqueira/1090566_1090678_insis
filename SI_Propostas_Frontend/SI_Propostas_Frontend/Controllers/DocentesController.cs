﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SI_Propostas_Frontend.Models;
using SI_Propostas_Backend;

namespace SI_Propostas_Frontend.Controllers
{
    public class DocentesController : Controller
    {
        private DocenteDBContext db = new DocenteDBContext();
        private ApiDocentes api = new ApiDocentes("localhost");

        // GET: /Docentes/
        public ActionResult Index()
        {
            List<Docente> docentesList = api.read().Select(a => new Docente
            {
                ID = a.ID,
                Email = a.Email,
                Sigla = a.Sigla,
                Nome = a.Nome,
            }).ToList();

            return View(docentesList.ToList());
            //return View(db.Docentes.ToList());
        }

        // GET: /Docentes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var docente_tmp = api.readById(Convert.ToInt32(id));
            Docente docente = new Docente
            {
                ID = docente_tmp.ID,
                Email = docente_tmp.Email,
                Nome = docente_tmp.Nome,
                Sigla = docente_tmp.Sigla,
            };
            
            //Docente docente = db.Docentes.Find(id);
            if (docente == null)
            {
                return HttpNotFound();
            }
            return View(docente);
        }

        // GET: /Docentes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Docentes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Nome,Sigla,Email")] Docente docente_tmp)
        {
            if (ModelState.IsValid)
            {
                SI_Propostas_Backend.Models.Docente docente = new SI_Propostas_Backend.Models.Docente
                {
                    ID = docente_tmp.ID,
                    Email = docente_tmp.Email,
                    Sigla = docente_tmp.Sigla,
                    Nome = docente_tmp.Nome
                };

                api.create(docente);

                //db.Docentes.Add(docente);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(docente_tmp);
        }

        // GET: /Docentes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var docente_tmp = api.readById(Convert.ToInt32(id));
            Docente docente = new Docente
            {
                ID = docente_tmp.ID,
                Email = docente_tmp.Email,
                Nome = docente_tmp.Nome,
                Sigla = docente_tmp.Sigla,
            };

            //Docente docente = db.Docentes.Find(id);
            if (docente == null)
            {
                return HttpNotFound();
            }
            return View(docente);
        }

        // POST: /Docentes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Nome,Sigla,Email")] Docente docente_tmp)
        {
            if (ModelState.IsValid)
            {
                SI_Propostas_Backend.Models.Docente docente = new SI_Propostas_Backend.Models.Docente
                {
                    ID = docente_tmp.ID,
                    Email = docente_tmp.Email,
                    Sigla = docente_tmp.Sigla,
                    Nome = docente_tmp.Nome
                };

                api.update(docente.ID, docente);

                //db.Entry(docente).State = EntityState.Modified;
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(docente_tmp);
        }

        // GET: /Docentes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var docente_tmp = api.readById(Convert.ToInt32(id));
            Docente docente = new Docente
            {
                ID = docente_tmp.ID,
                Email = docente_tmp.Email,
                Nome = docente_tmp.Nome,
                Sigla = docente_tmp.Sigla,
            };
            
            //Docente docente = db.Docentes.Find(id);
            if (docente == null)
            {
                return HttpNotFound();
            }
            return View(docente);
        }

        // POST: /Docentes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var docente_tmp = api.readById(Convert.ToInt32(id));
            Docente docente = new Docente
            {
                ID = docente_tmp.ID,
                Email = docente_tmp.Email,
                Nome = docente_tmp.Nome,
                Sigla = docente_tmp.Sigla,
            };

            api.delete(Convert.ToInt32(id));

            //Docente docente = db.Docentes.Find(id);
            //db.Docentes.Remove(docente);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
