﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SI_Propostas_Frontend.Models;
using SI_Propostas_Backend;

namespace SI_Propostas_Frontend.Controllers
{
    public class OrganizacoesController : Controller
    {
        private OrganizacaoDBContext db = new OrganizacaoDBContext();
        private ApiOrganizacoes api = new ApiOrganizacoes("localhost");
        // GET: /Organizacoes/
        public ActionResult Index()
        {
            List<Organizacao> organizacoesList = api.read().Select(a => new Organizacao
            {
                ID = a.id,
                Email = a.email,
                Morada = a.morada,
                Designacao = a.designacao,
                Telefone = a.telefone 
            }).ToList();

            return View(organizacoesList.ToList());
            //return View(db.Organizacoes.ToList());
        }

        // GET: /Organizacoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            var organizacao_tmp = api.readById(Convert.ToInt32(id));
            Organizacao organizacao = new Organizacao
            {
                ID = organizacao_tmp.id,
                Email = organizacao_tmp.email,
                Morada = organizacao_tmp.morada,
                Designacao = organizacao_tmp.designacao,
                Telefone = organizacao_tmp.telefone 
            };

            //Organizacao organizacao = db.Organizacoes.Find(id);
            if (organizacao == null)
            {
                return HttpNotFound();
            }
            return View(organizacao);
        }

        // GET: /Organizacoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Organizacoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Designacao,Morada,Telefone,Email")] Organizacao organizacao_tmp)
        {
            if (ModelState.IsValid)
            {
                SI_Propostas_Backend.Models.Organizacao organizacao = new SI_Propostas_Backend.Models.Organizacao
                {
                    id = organizacao_tmp.ID,
                    email = organizacao_tmp.Email,
                    morada = organizacao_tmp.Morada,
                    designacao = organizacao_tmp.Designacao,
                    telefone = organizacao_tmp.Telefone
                };

                List<int> organizacoesList = api.read().Select(a => new Organizacao
                {
                    ID = a.id,
                    Email = a.email,
                    Morada = a.morada,
                    Designacao = a.designacao,
                    Telefone = a.telefone
                }).ToList().Select(i => i.ID).ToList();

                if (organizacoesList.Any())
                {
                    int max = organizacoesList.Count;
                    organizacao.id = ++max;
                }
                else
                {
                    organizacao.id = 1;
                }
                api.create(organizacao);

                //db.Organizacoes.Add(organizacao);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(organizacao_tmp);
        }

        // GET: /Organizacoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var organizacao_tmp = api.readById(Convert.ToInt32(id));
            Organizacao organizacao = new Organizacao
            {
                ID = organizacao_tmp.id,
                Email = organizacao_tmp.email,
                Morada = organizacao_tmp.morada,
                Designacao = organizacao_tmp.designacao,
                Telefone = organizacao_tmp.telefone
            };

            //Organizacao organizacao = db.Organizacoes.Find(id);
            if (organizacao == null)
            {
                return HttpNotFound();
            }
            return View(organizacao);
        }

        // POST: /Organizacoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Designacao,Morada,Telefone,Email")] Organizacao organizacao_tmp)
        {
            if (ModelState.IsValid)
            {
                SI_Propostas_Backend.Models.Organizacao organizacao = new SI_Propostas_Backend.Models.Organizacao
                {
                    id = organizacao_tmp.ID,
                    email = organizacao_tmp.Email,
                    morada = organizacao_tmp.Morada,
                    designacao = organizacao_tmp.Designacao,
                    telefone = organizacao_tmp.Telefone
                };

                api.update(organizacao.id, organizacao);

                //db.Entry(organizacao).State = EntityState.Modified;
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(organizacao_tmp);
        }

        // GET: /Organizacoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var organizacao_tmp = api.readById(Convert.ToInt32(id));
            Organizacao organizacao = new Organizacao
            {
                ID = organizacao_tmp.id,
                Email = organizacao_tmp.email,
                Morada = organizacao_tmp.morada,
                Designacao = organizacao_tmp.designacao,
                Telefone = organizacao_tmp.telefone
            };

            //Organizacao organizacao = db.Organizacoes.Find(id);
            if (organizacao == null)
            {
                return HttpNotFound();
            }
            return View(organizacao);
        }

        // POST: /Organizacoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var organizacao_tmp = api.readById(Convert.ToInt32(id));
            Organizacao organizacao = new Organizacao
            {
                ID = organizacao_tmp.id,
                Email = organizacao_tmp.email,
                Morada = organizacao_tmp.morada,
                Designacao = organizacao_tmp.designacao,
                Telefone = organizacao_tmp.telefone
            };

            api.delete(organizacao.ID);

            //Organizacao organizacao = db.Organizacoes.Find(id);
            //db.Organizacoes.Remove(organizacao);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
