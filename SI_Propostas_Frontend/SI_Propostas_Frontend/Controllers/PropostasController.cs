﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SI_Propostas_Frontend.Models;
using SI_Propostas_Backend;

namespace SI_Propostas_Frontend.Controllers
{
    public class PropostasController : Controller
    {
        private PropostaDBContext db = new PropostaDBContext();
        private ApiPropostas api = new ApiPropostas();

        // GET: /Propostas/
        public ActionResult Index()
        {
            List<Proposta> propostasList = api.read().Select(a => new Proposta
            {
                ID = a.ID,
                Submissao = a.Submissao,
                Titulo = a.Titulo,
                Designacao = a.Designacao,
                Objectivos = a.Objectivos,
                Periodo = a.Periodo
            }).ToList();

            return View(propostasList.ToList());            
            
            //return View(db.Propostas.ToList());
        }

        // GET: /Propostas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var proposta_tmp = api.readById(Convert.ToInt32(id));
            Proposta proposta = new Proposta
            {
                ID = proposta_tmp.ID,
                Submissao = proposta_tmp.Submissao,
                Titulo = proposta_tmp.Titulo,
                Designacao = proposta_tmp.Designacao,
                Objectivos = proposta_tmp.Objectivos,
                Periodo = proposta_tmp.Periodo
            };

            //Proposta proposta = db.Propostas.Find(id);
            if (proposta == null)
            {
                return HttpNotFound();
            }
            return View(proposta);
        }

        // GET: /Propostas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Propostas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Submissao,Titulo,Designacao,Objectivos,Periodo")] Proposta proposta_tmp)
        {
            if (ModelState.IsValid)
            {
                SI_Propostas_Backend.Models.Proposta proposta = new SI_Propostas_Backend.Models.Proposta
                {
                    ID = proposta_tmp.ID,
                    Submissao = proposta_tmp.Submissao,
                    Titulo = proposta_tmp.Titulo,
                    Designacao = proposta_tmp.Designacao,
                    Objectivos = proposta_tmp.Objectivos,
                    Periodo = proposta_tmp.Periodo
                };

                ApiOrganizacoes api_org = new ApiOrganizacoes("localhost");
                ApiDocentes api_doc = new ApiDocentes("localhost");

                List<string> organizacoesList = api_org.read().Select(a => new Organizacao
                {
                    ID = a.id,
                    Email = a.email,
                    Morada = a.morada,
                    Designacao = a.designacao,
                    Telefone = a.telefone
                }).ToList().Select(d => d.Designacao).ToList();

                List<string> docentesList = api_doc.read().Select(a => new Docente
                {
                    ID = a.ID,
                    Email = a.Email,
                    Sigla = a.Sigla,
                    Nome = a.Nome,
                }).ToList().Select(d => d.Nome).ToList();

                if (organizacoesList.Contains(proposta.Submissao) || docentesList.Contains(proposta.Submissao)) {
                    api.create(proposta);
                }
                else
                {
                    return View(proposta_tmp);
                }

                //db.Propostas.Add(proposta);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(proposta_tmp);
        }

        // GET: /Propostas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var proposta_tmp = api.readById(Convert.ToInt32(id));
            Proposta proposta = new Proposta
            {
                ID = proposta_tmp.ID,
                Submissao = proposta_tmp.Submissao,
                Titulo = proposta_tmp.Titulo,
                Designacao = proposta_tmp.Designacao,
                Objectivos = proposta_tmp.Objectivos,
                Periodo = proposta_tmp.Periodo
            };

            //Proposta proposta = db.Propostas.Find(id);
            if (proposta == null)
            {
                return HttpNotFound();
            }
            return View(proposta);
        }

        // POST: /Propostas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Submissao,Titulo,Designacao,Objectivos,Periodo")] Proposta proposta_tmp)
        {
            if (ModelState.IsValid)
            {
                SI_Propostas_Backend.Models.Proposta proposta = new SI_Propostas_Backend.Models.Proposta
                {
                    ID = proposta_tmp.ID,
                    Submissao = proposta_tmp.Submissao,
                    Titulo = proposta_tmp.Titulo,
                    Designacao = proposta_tmp.Designacao,
                    Objectivos = proposta_tmp.Objectivos,
                    Periodo = proposta_tmp.Periodo
                };

                ApiOrganizacoes api_org = new ApiOrganizacoes("localhost");
                ApiDocentes api_doc = new ApiDocentes("localhost");

                List<string> organizacoesList = api_org.read().Select(a => new Organizacao
                {
                    ID = a.id,
                    Email = a.email,
                    Morada = a.morada,
                    Designacao = a.designacao,
                    Telefone = a.telefone
                }).ToList().Select(d => d.Designacao).ToList();

                List<string> docentesList = api_doc.read().Select(a => new Docente
                {
                    ID = a.ID,
                    Email = a.Email,
                    Sigla = a.Sigla,
                    Nome = a.Nome,
                }).ToList().Select(d => d.Nome).ToList();

                if (organizacoesList.Contains(proposta.Submissao) || docentesList.Contains(proposta.Submissao))
                {
                    api.update(proposta.ID, proposta);
                }
                else
                {
                    return View(proposta_tmp);
                }

                //db.Entry(proposta).State = EntityState.Modified;
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(proposta_tmp);
        }

        // GET: /Propostas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var proposta_tmp = api.readById(Convert.ToInt32(id));
            Proposta proposta = new Proposta
            {
                ID = proposta_tmp.ID,
                Submissao = proposta_tmp.Submissao,
                Titulo = proposta_tmp.Titulo,
                Designacao = proposta_tmp.Designacao,
                Objectivos = proposta_tmp.Objectivos,
                Periodo = proposta_tmp.Periodo
            };

            //Proposta proposta = db.Propostas.Find(id);
            if (proposta == null)
            {
                return HttpNotFound();
            }
            return View(proposta);
        }

        // POST: /Propostas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var proposta_tmp = api.readById(Convert.ToInt32(id));
            Proposta proposta = new Proposta
            {
                ID = proposta_tmp.ID,
                Submissao = proposta_tmp.Submissao,
                Titulo = proposta_tmp.Titulo,
                Designacao = proposta_tmp.Designacao,
                Objectivos = proposta_tmp.Objectivos,
                Periodo = proposta_tmp.Periodo
            };

            api.delete(Convert.ToInt32(id));

            //Proposta proposta = db.Propostas.Find(id);
            //db.Propostas.Remove(proposta);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
