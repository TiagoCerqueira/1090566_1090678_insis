﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SI_Propostas_Frontend.Models;
using SI_Propostas_Frontend;
using SI_Propostas_Backend;

namespace SI_Propostas_Frontend.Controllers
{
    public class AlunosController : Controller
    {
        private AlunoDBContext db = new AlunoDBContext();
        private ApiAlunos api = new ApiAlunos();
        // GET: /Alunos/
        public ActionResult Index()
        {
            List<Aluno> alunosList = api.read().Select(a => new Aluno {
                ID = a.ID, 
                Email = a.Email,
                Morada = a.Morada,
                Nome = a.Nome,
                Numero_Mecanografico = a.Numero_Mecanografico,
                Telefone = a.Telefone
            }).ToList();

            return View(alunosList.ToList());
            //return View(db.Alunos.ToList());
        }

        // GET: /Alunos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            string numMec = Convert.ToString(id);
            var aluno_tmp = api.readById(numMec);
            Aluno aluno = new Aluno
            {
                ID = aluno_tmp.ID,
                Email = aluno_tmp.Email,
                Morada = aluno_tmp.Morada,
                Nome = aluno_tmp.Nome,
                Numero_Mecanografico = aluno_tmp.Numero_Mecanografico,
                Telefone = aluno_tmp.Telefone
            };

            //Aluno aluno = db.Alunos.Find(id);
            if (aluno == null)
            {
                return HttpNotFound();
            }
            return View(aluno);
        }

        // GET: /Alunos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Alunos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Numero_Mecanografico,Nome,Morada,Telefone,Email")] Aluno aluno_tmp)
        {
            if (ModelState.IsValid)
            {
                SI_Propostas_Backend.Models.Aluno aluno = new SI_Propostas_Backend.Models.Aluno
                {
                    ID = aluno_tmp.ID,
                    Email = aluno_tmp.Email,
                    Morada = aluno_tmp.Morada,
                    Nome = aluno_tmp.Nome,
                    Numero_Mecanografico = aluno_tmp.Numero_Mecanografico,
                    Telefone = aluno_tmp.Telefone
                };

                api.create(aluno);
                //db.Alunos.Add(aluno);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aluno_tmp);
        }

        // GET: /Alunos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            string numMec = Convert.ToString(id);
            var aluno_tmp = api.readById(numMec);
            Aluno aluno = new Aluno
            {
                ID = aluno_tmp.ID,
                Email = aluno_tmp.Email,
                Morada = aluno_tmp.Morada,
                Nome = aluno_tmp.Nome,
                Numero_Mecanografico = aluno_tmp.Numero_Mecanografico,
                Telefone = aluno_tmp.Telefone
            };
            //Aluno aluno = db.Alunos.Find(id);
            if (aluno == null)
            {
                return HttpNotFound();
            }
            return View(aluno);
        }

        // POST: /Alunos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Numero_Mecanografico,Nome,Morada,Telefone,Email")] Aluno aluno_tmp)
        {
            if (ModelState.IsValid)
            {
                SI_Propostas_Backend.Models.Aluno aluno = new SI_Propostas_Backend.Models.Aluno
                {
                    ID = aluno_tmp.ID,
                    Email = aluno_tmp.Email,
                    Morada = aluno_tmp.Morada,
                    Nome = aluno_tmp.Nome,
                    Numero_Mecanografico = aluno_tmp.Numero_Mecanografico,
                    Telefone = aluno_tmp.Telefone
                };

                api.update(aluno.Numero_Mecanografico, aluno);

                //db.Entry(aluno).State = EntityState.Modified;
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aluno_tmp);
        }

        // GET: /Alunos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            string numMec = Convert.ToString(id);
            var aluno_tmp = api.readById(numMec);
            Aluno aluno = new Aluno
            {
                ID = aluno_tmp.ID,
                Email = aluno_tmp.Email,
                Morada = aluno_tmp.Morada,
                Nome = aluno_tmp.Nome,
                Numero_Mecanografico = aluno_tmp.Numero_Mecanografico,
                Telefone = aluno_tmp.Telefone
            };
            
            //Aluno aluno = db.Alunos.Find(id);
            if (aluno == null)
            {
                return HttpNotFound();
            }
            return View(aluno);
        }

        // POST: /Alunos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            string numMec = Convert.ToString(id);
            var aluno_tmp = api.readById(numMec);
            Aluno aluno = new Aluno
            {
                ID = aluno_tmp.ID,
                Email = aluno_tmp.Email,
                Morada = aluno_tmp.Morada,
                Nome = aluno_tmp.Nome,
                Numero_Mecanografico = aluno_tmp.Numero_Mecanografico,
                Telefone = aluno_tmp.Telefone
            };

            api.delete(aluno.Numero_Mecanografico);
            //Aluno aluno = db.Alunos.Find(id);
            //db.Alunos.Remove(aluno);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
