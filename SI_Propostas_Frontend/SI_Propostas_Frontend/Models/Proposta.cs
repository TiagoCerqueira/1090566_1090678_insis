﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SI_Propostas_Frontend.Models
{
    public class Proposta
    {
        public int ID { get; set; }
        public string Submissao { get; set; }
        public string Titulo { get; set; }
        public string Designacao { get; set; }
        public string Objectivos { get; set; }
        public string Periodo { get; set; }
    }

    public class PropostaDBContext : DbContext
    {
        public DbSet<Proposta> Propostas { get; set; }
    }
}