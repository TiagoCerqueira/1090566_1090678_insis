﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SI_Propostas_Frontend.Models
{
    public class Aluno
    {
        public int ID { get; set; }
        [DisplayName("Nº Mecanografico")]
        public string Numero_Mecanografico { get; set; }
        public string Nome { get; set; }
        public string Morada { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
    }

    public class AlunoDBContext : DbContext
    {
        public DbSet<Aluno> Alunos { get; set; }
    }
}