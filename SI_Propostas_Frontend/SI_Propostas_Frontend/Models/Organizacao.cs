﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SI_Propostas_Frontend.Models
{
    public class Organizacao
    {
        public int ID { get; set; }
        public string Designacao { get; set; }
        public string Morada { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
    }

    public class OrganizacaoDBContext : DbContext
    {
        public DbSet<Organizacao> Organizacoes { get; set; }
    }
}