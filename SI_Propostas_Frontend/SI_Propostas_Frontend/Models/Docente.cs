﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SI_Propostas_Frontend.Models
{
    public class Docente
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public string Email { get; set; }
    }

    public class DocenteDBContext : DbContext
    {
        public DbSet<Docente> Docentes { get; set; }
    }
}