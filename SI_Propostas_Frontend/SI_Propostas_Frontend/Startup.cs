﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SI_Propostas_Frontend.Startup))]
namespace SI_Propostas_Frontend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
