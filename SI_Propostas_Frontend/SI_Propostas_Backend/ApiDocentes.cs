﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using SI_Propostas_Backend.Models;
using Newtonsoft.Json;

namespace SI_Propostas_Backend
{
    public class ApiDocentes
    {
        string host;
        RestClient client;
        RestRequest req;
        List<Docente> docenteList;

        public ApiDocentes(string host)
        {
            this.host = host;
            client = new RestClient("http://" + this.host + ":49708");
        }
       
        public List<Docente> read()
        {
            req = new RestRequest("api/docentes/", Method.GET);
            IRestResponse response = client.Execute(req);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                //Print error
                //response.StatusDescription
                return docenteList;
            }
            else
            {
                string content = response.Content;
                docenteList = new List<Docente>();
                docenteList = JsonConvert.DeserializeObject<List<Docente>>(content);
                return docenteList;
            }
        }

        public Docente readById(int id)
        {
            req = new RestRequest("api/docentes/{id}", Method.GET);
            req.AddParameter("name", "value");
            req.AddUrlSegment("id", id.ToString());
            req.AddHeader("header", "value");
            IRestResponse response = client.Execute(req);
            if ((int)response.StatusCode != 200)
            {
                //Print error
                //response.StatusDescription
                return null;
            }
            else
            {
                string content = response.Content;
                Docente docente = JsonConvert.DeserializeObject<Docente>(content);
                return docente;
            }
        }

        public bool create(Docente docente)
        {
            req = new RestRequest("api/docentes/", Method.POST);
            req.RequestFormat = DataFormat.Json;
            req.AddBody(docente);
            IRestResponse response = client.Execute(req);
            ///ERROR HANDLING!!!!response.StatusCode
            return true;
        }

        public bool delete(int id)
        {
            req = new RestRequest("api/docentes/{id}", Method.DELETE);
            req.AddParameter("name", "value");
            req.AddUrlSegment("id", id.ToString());
            IRestResponse response = client.Execute(req);
            ///ERROR HANDLING!!!!response.StatusCode
            return true;
        }

        public bool update(int id, Docente docente)
        {
            req = new RestRequest("api/docentes/{id}", Method.PUT);
            req.RequestFormat = DataFormat.Json;
            req.AddUrlSegment("id", id.ToString());
            req.AddBody(docente);
            IRestResponse response = client.Execute(req);
            ///ERROR HANDLING!!!!response.StatusCode
            return true;
        }
    }
}
