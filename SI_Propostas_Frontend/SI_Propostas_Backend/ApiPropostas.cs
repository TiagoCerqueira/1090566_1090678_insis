﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SI_Propostas_Backend.Models;
using SI_Propostas_Backend.DAL;
namespace SI_Propostas_Backend
{
    public class ApiPropostas
    {
        DAL.PropostasDal dal;
        public ApiPropostas()
        {
            dal = new DAL.PropostasDal();
        }

        public List<Proposta> read()
        {
            return dal.read();   
        }

        public Proposta readById(int id)
        {
            Proposta prop = dal.readById(id);
            return prop;
        }

        public bool create(Proposta proposta)
        {
            return dal.create(proposta);
        }

        public bool delete(int id)
        {
            return dal.delete(id);
        }

        public bool update(int id, Proposta proposta)
        {
            return dal.update(id, proposta);
        }
    }
}
