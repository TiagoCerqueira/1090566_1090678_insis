﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI_Propostas_Backend.Models
{
    public class Organizacao
    {
        public int id { get; set; }
        public string designacao { get; set; }
        public string morada { get; set; }
        public string telefone { get; set; }
        public string email { get; set; }

        public Organizacao()
        {

        }
        public Organizacao(int id, string designacao, string morada, string email, string telefone)
        {
            this.id = id;
            this.designacao = designacao;
            this.morada = morada;
            this.telefone = telefone;
            this.email = email;
        }

    }
}