﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI_Propostas_Backend.Models
{
    public class Docente
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public string Email { get; set; }

        public Docente()
        {

        }

        public Docente(int ID, string Nome, string Sigla, string Email)
        {
            this.ID = ID;
            this.Nome = Nome;
            this.Sigla = Sigla;
            this.Email = Email;
        }
    }
}