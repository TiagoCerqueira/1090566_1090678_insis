﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI_Propostas_Backend.Models
{
    public class Proposta
    {
        public int ID { get; set; }
        public string Submissao { get; set; }
        public string Titulo { get; set; }
        public string Designacao { get; set; }
        public string Objectivos { get; set; }
        public string Periodo { get; set; }
    
        public Proposta()
        {

        }

        public Proposta(int ID, string Submissao, string Titulo, string Designacao, string Objectivos, string Periodo)
        {
            this.ID = ID;
            this.Submissao = Submissao;
            this.Titulo = Titulo;
            this.Designacao = Designacao;
            this.Objectivos = Objectivos;
            this.Periodo = Periodo;
        }
    }
}
