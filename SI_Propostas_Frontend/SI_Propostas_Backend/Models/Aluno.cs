﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SI_Propostas_Backend.Models
{
    public class Aluno
    {
        public int ID { get; set; }
        [DisplayName("Nº Mecanografico")]
        public string Numero_Mecanografico { get; set; }
        public string Nome { get; set; }
        public string Morada { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        
        public Aluno()
        {

        }

        public Aluno(string Numero_Mecanografico, string Nome, string Morada, string Email, string Telefone)
        {
            this.ID = Int32.Parse(Numero_Mecanografico);
            this.Numero_Mecanografico = Numero_Mecanografico;
            this.Nome = Nome;
            this.Morada = Morada;
            this.Email = Email;
            this.Telefone = Telefone;
        }


    }

}