﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using SI_Propostas_Backend.Models;
namespace SI_Propostas_Backend
{
    public class ApiAlunos
    {
        AlunosAPI.AlunosAPIClient api;
        public ApiAlunos()
        {
           BasicHttpBinding binding = new BasicHttpBinding();
           EndpointAddress address = new EndpointAddress(new Uri("http://192.168.56.2:8080/SI_Alunos/AlunosAPI"));
           api = new AlunosAPI.AlunosAPIClient(binding, address);
        }
        
        public bool create(Aluno al)
        {
            bool flag= api.create(Int32.Parse(al.Numero_Mecanografico), al.Nome, al.Morada, al.Email, Int32.Parse(al.Telefone));
            return flag;
        }
        
        public bool update(string numMec,Aluno al)
        {
            bool flag = api.update(Int32.Parse(numMec), al.Nome, al.Morada, al.Email, Double.Parse(al.Telefone));
            return flag;
        }
        
        public List<Aluno> read()
        {
            AlunosAPI.alunos[] listAl = api.read();
            List<Aluno> listAlunos = new List<Aluno>();
            foreach(AlunosAPI.alunos al in listAl)
            {
                listAlunos.Add(new Aluno(al.nummec.ToString(), al.nome, al.morada, al.email, al.telefone.ToString()));
            }
            return listAlunos;
        }

        public Aluno readById(string numMec)
        {
            AlunosAPI.alunos[] listAl = api.readById(Int32.Parse(numMec));
            Aluno al = new Aluno(listAl[0].nummec.ToString(), listAl[0].nome, listAl[0].morada, listAl[0].email, listAl[0].telefone.ToString());
            return al;
        }

        public bool delete(string numMec)
        {
            bool flag = api.delete(Int32.Parse(numMec));
            return flag;
        }

    }
}
