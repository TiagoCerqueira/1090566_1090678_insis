﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Sql;
using System.Data;
using SI_Propostas_Backend.Models;
namespace SI_Propostas_Backend.DAL
{
    public class PropostasDal
    {
        SqlConnection conn;
        public PropostasDal()
        {
            conn = new SqlConnection("Data Source=(localdb)\\Projects;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False");
        }

        private string GetConnectionString()
        {
            //Util-2 Assume failure.
            string returnValue = null;

            //Util-3 Look for the name in the connectionStrings section.
            ConnectionStringSettings settings =
            ConfigurationManager.ConnectionStrings["SI_Propostas_Backend.Properties.Settings.connString"];

            //If found, return the connection string.
            if (settings != null)
                returnValue = settings.ConnectionString;

            return returnValue;
        }

        public bool create (Proposta proposta)
        {
                string sql = "INSERT INTO dbo.Propostas VALUES (@titulo,@designacao,@objectivos,@periodo,@submissao)";
                //Create a SqlCommand object.
                SqlCommand command = new SqlCommand(sql, conn);
                
                //Define the @orderID parameter and its value.
                command.Parameters.Add(new SqlParameter("@titulo", SqlDbType.VarChar));
                command.Parameters.Add(new SqlParameter("@designacao", SqlDbType.VarChar));
                command.Parameters.Add(new SqlParameter("@objectivos", SqlDbType.VarChar));
                command.Parameters.Add(new SqlParameter("@periodo", SqlDbType.VarChar));
                command.Parameters.Add(new SqlParameter("@submissao", SqlDbType.VarChar));   
                command.Parameters["@titulo"].Value = proposta.Titulo;
                command.Parameters["@designacao"].Value = proposta.Designacao;
                command.Parameters["@objectivos"].Value = proposta.Objectivos;
                command.Parameters["@periodo"].Value = proposta.Periodo;
                command.Parameters["@submissao"].Value = proposta.Submissao;
                //try – catch - finally
                try
                {
                    conn.Open();
                    command.ExecuteNonQuery();
                }catch
                {

                }finally
                {
                    conn.Close();
                }
            return true;
        }

        public List<Proposta> read()
        {
            string sql = "select * from dbo.Propostas";
            SqlCommand command = new SqlCommand(sql, conn);
            List<Proposta> propostas = new List<Proposta>();
            
            try
            {
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                while(reader.Read())
                {
                    Proposta p = new Proposta();
                    p.ID = Convert.ToInt32(reader["Id"]);
                    p.Titulo = reader["Titulo"].ToString();
                    p.Submissao = reader["Submissao"].ToString();
                    p.Objectivos = reader["Objectivos"].ToString();
                    p.Designacao = reader["Descricao"].ToString();
                    p.Periodo = reader["Periodo"].ToString();
                    propostas.Add(p);
                }
            }catch
            {

            }finally
            {
                conn.Close();
            }
            return propostas;
        }

        public Proposta readById(int id)
        {
            string sql = "select * from dbo.Propostas where Id=@id";
            SqlCommand command = new SqlCommand(sql, conn);
            command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int));
            command.Parameters["@id"].Value = id;
                
            List<Proposta> propostas = new List<Proposta>();

            try
            {
                conn.Open();
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Proposta p = new Proposta();
                    p.ID = Convert.ToInt32(reader["Id"]);
                    p.Titulo = reader["Titulo"].ToString();
                    p.Submissao = reader["Submissao"].ToString();
                    p.Objectivos = reader["Objectivos"].ToString();
                    p.Designacao = reader["Descricao"].ToString();
                    p.Periodo = reader["Periodo"].ToString();
                    propostas.Add(p);
                }
            }
            catch
            {

            }
            finally
            {
                conn.Close();
            }
            return propostas[0];
        }

        public bool delete(int id)
        {
            string sql = "DELETE FROM dbo.Propostas WHERE Id=@id";
            //Create a SqlCommand object.
            SqlCommand command = new SqlCommand(sql, conn);

            //Define the @orderID parameter and its value.
            command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int));
            command.Parameters["@id"].Value = id;
            //try – catch - finally
            try
            {
                conn.Open();
                command.ExecuteNonQuery();
            }
            catch
            {

            }
            finally
            {
                conn.Close();
            }
            return true;
        }
        public bool update(int id, SI_Propostas_Backend.Models.Proposta proposta)
        {
            string sql = "UPDATE dbo.Propostas SET Titulo=@titulo,Descricao=@designacao,Objectivos=@objectivos,Periodo=@periodo,Submissao=@submissao WHERE Id=@id";
            //Create a SqlCommand object.
            SqlCommand command = new SqlCommand(sql, conn);

            //Define the @orderID parameter and its value.
            command.Parameters.Add(new SqlParameter("@id", SqlDbType.Int));
            command.Parameters.Add(new SqlParameter("@titulo", SqlDbType.VarChar));
            command.Parameters.Add(new SqlParameter("@designacao", SqlDbType.VarChar));
            command.Parameters.Add(new SqlParameter("@objectivos", SqlDbType.VarChar));
            command.Parameters.Add(new SqlParameter("@periodo", SqlDbType.VarChar));
            command.Parameters.Add(new SqlParameter("@submissao", SqlDbType.VarChar));
            command.Parameters["@id"].Value = id;
            command.Parameters["@titulo"].Value = proposta.Titulo;
            command.Parameters["@designacao"].Value = proposta.Designacao;
            command.Parameters["@objectivos"].Value = proposta.Objectivos;
            command.Parameters["@periodo"].Value = proposta.Periodo;
            command.Parameters["@submissao"].Value = proposta.Submissao;
            //try – catch - finally
            try
            {
                conn.Open();
                command.ExecuteNonQuery();
            }
            catch
            {

            }
            finally
            {
                conn.Close();
            }
            return true;
        }

    }
}
