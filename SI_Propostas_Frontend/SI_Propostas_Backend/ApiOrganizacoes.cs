﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using SI_Propostas_Backend.Models;
using Newtonsoft.Json;
namespace SI_Propostas_Backend
{
    public class ApiOrganizacoes
    {
        string host;
        RestClient client;
        RestRequest req;
        List<Organizacao> orgList;

        public ApiOrganizacoes(string host)
        {
            this.host = host;
            client = new RestClient("http://" + this.host + ":8080");
        }
       
        public List<Organizacao> read()
        {
            req = new RestRequest("SI_Organizacoes/webresources/entities.organizacoes/", Method.GET);
            IRestResponse response = client.Execute(req);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                //Print error
                //response.StatusDescription
                return orgList;
            }
            else
            {
                string content = response.Content;
                orgList = new List<Organizacao>();
                orgList = JsonConvert.DeserializeObject<List<Organizacao>>(content);
                return orgList;
            }
        }

        public Organizacao readById(int id)
        {
            req = new RestRequest("SI_Organizacoes/webresources/entities.organizacoes/{id}", Method.GET);
            req.AddParameter("name", "value");
            req.AddUrlSegment("id", id.ToString());
            req.AddHeader("header", "value");
            IRestResponse response = client.Execute(req);
            if ((int)response.StatusCode != 200)
            {
                //Print error
                //response.StatusDescription
                return null;
            }
            else
            {
                string content = response.Content;
                Organizacao org = JsonConvert.DeserializeObject<Organizacao>(content);
                return org;
            }
        }

        public bool create(Organizacao org)
        {
            req = new RestRequest("SI_Organizacoes/webresources/entities.organizacoes/", Method.POST);
            req.RequestFormat = DataFormat.Json;
            req.AddBody(org);
            IRestResponse response = client.Execute(req);
            ///ERROR HANDLING!!!!response.StatusCode
            return true;
        }

        public bool delete(int id)
        {
            req = new RestRequest("SI_Organizacoes/webresources/entities.organizacoes/{id}", Method.DELETE);
            req.AddParameter("name", "value");
            req.AddUrlSegment("id", id.ToString());
            IRestResponse response = client.Execute(req);
            ///ERROR HANDLING!!!!response.StatusCode
            return true;
        }

        public bool update(int id, Organizacao org)
        {
            req = new RestRequest("SI_Organizacoes/webresources/entities.organizacoes/{id}", Method.PUT);
            req.RequestFormat = DataFormat.Json;
            req.AddUrlSegment("id", id.ToString());
            req.AddBody(org);
            IRestResponse response = client.Execute(req);
            ///ERROR HANDLING!!!!response.StatusCode
            return true;
        }
    }
}
