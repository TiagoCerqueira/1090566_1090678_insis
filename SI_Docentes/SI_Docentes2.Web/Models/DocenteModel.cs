﻿using SI_Docentes2.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SI_Docentes2.Web.Models
{
    public class DocenteModel
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public string Email { get; set; }
    }
}