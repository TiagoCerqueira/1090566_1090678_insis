﻿using SI_Docentes2.Data;
using SI_Docentes2.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Routing;

namespace SI_Docentes2.Web.Models
{
    public class ModelFactory
    {
        private System.Web.Http.Routing.UrlHelper _UrlHelper;

        public ModelFactory(HttpRequestMessage request)
        {
            _UrlHelper = new System.Web.Http.Routing.UrlHelper(request);
        }

        public DocenteModel Create(Docente docente)
        {
            return new DocenteModel()
            {
                Id = docente.Id,
                Url = _UrlHelper.Link("Docentes", new { id = docente.Id }),
                Nome = docente.Nome,
                Sigla = docente.Sigla,
                Email = docente.Email
            };
        }

        public Docente Parse(DocenteModel model)
        {
            try
            {
                var docente = new Docente()
                   {
                       Id = model.Id,
                       Nome = model.Nome,
                       Sigla = model.Sigla,
                       Email = model.Email
                   };

                return docente;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}