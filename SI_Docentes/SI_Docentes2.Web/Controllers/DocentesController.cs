﻿using SI_Docentes2.Data;
using SI_Docentes2.Data.Entities;
using SI_Docentes2.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SI_Docentes2.Web.Controllers
{
    public class DocentesController : BaseApiController
    {
        public DocentesController(IDocentesRepository repo)
            : base(repo)
        {
        }

        public IEnumerable<DocenteModel> Get()
        {
            IQueryable<Docente> query;

            query = TheRepository.GetAllDocentes();
 
            var results = query
            .ToList()
            .Select(s => TheModelFactory.Create(s));
 
            return results;
        }
 
        public HttpResponseMessage GetDocente(int id)
        {
            try
            {
                var course = TheRepository.GetDocente(id);
                if (course != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, TheModelFactory.Create(course));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
 
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Post([FromBody] DocenteModel docenteModel)
        {
            try
            {
                var entity = TheModelFactory.Parse(docenteModel);

                if (entity == null) Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Could not read subject/docente from body");

                if (TheRepository.Insert(entity) && TheRepository.SaveAll())
                {
                    return Request.CreateResponse(HttpStatusCode.Created, TheModelFactory.Create(entity));
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Could not save to the database.");
                }
            }
            catch (Exception ex)
            {

                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPatch]
        [HttpPut]
        public HttpResponseMessage Put(int id, [FromBody] DocenteModel docenteModel)
        {
            try
            {

                var updatedDocente = TheModelFactory.Parse(docenteModel);

                if (updatedDocente == null) Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Could not read subject/tutor from body");

                var originalDocente = TheRepository.GetDocente(id);

                if (originalDocente == null || originalDocente.Id != id)
                {
                    return Request.CreateResponse(HttpStatusCode.NotModified, "Course is not found");
                }
                else
                {
                    updatedDocente.Id = id;
                }

                if (TheRepository.Update(originalDocente, updatedDocente) && TheRepository.SaveAll())
                {
                    return Request.CreateResponse(HttpStatusCode.OK, TheModelFactory.Create(updatedDocente));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotModified);
                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var docente = TheRepository.GetDocente(id);

                if (docente == null)
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

                if (TheRepository.DeleteDocente(id) && TheRepository.SaveAll())
                {
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
