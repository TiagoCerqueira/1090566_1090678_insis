﻿using SI_Docentes2.Data;
using SI_Docentes2.Data.Entities;
using SI_Docentes2.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SI_Docentes2.Web.Controllers
{
    public class BaseApiController : ApiController
    {
        private IDocentesRepository _repo;
        private ModelFactory _modelFactory;

        public BaseApiController(IDocentesRepository repo)
        {
            _repo = repo;
        }

        protected ModelFactory TheModelFactory
        {
            get
            {
                if (_modelFactory == null)
                {
                    _modelFactory = new ModelFactory(Request);
                }
                return _modelFactory;
            }
        }

        protected IDocentesRepository TheRepository
        {
            get
            {
                return _repo;
            }
        }
    }
}
