﻿using SI_Docentes2.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI_Docentes2.Data
{
    class DocentesDataSeeder
    {
        DocentesContext _ctx;
        public DocentesDataSeeder(DocentesContext ctx)
        {
            _ctx = ctx;
        }

        public void Seed()
        {
            try
            {
                for (int i = 0; i < docentesNames.Length; i++)
                {
                    var nameSiglaMail = SplitValue(docentesNames[i]);
                    var docente = new Docente
                    {
                        Nome = nameSiglaMail[0],
                        Sigla = nameSiglaMail[1],
                        Email = nameSiglaMail[2],
                    };

                    _ctx.Docentes.Add(docente);
                }

                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                string message = ex.ToString(); 
                throw ex;
            }
        }

        private static string[] SplitValue(string val)
        {
            return val.Split(',');
        }

        private string RandomString(int size)
        {
            Random _rng = new Random((int)DateTime.Now.Ticks);
            string _chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            char[] buffer = new char[size];

            for (int i = 0; i < size; i++)
            {
                buffer[i] = _chars[_rng.Next(_chars.Length)];
            }
            return new string(buffer);
        }

        static string[] docentesNames = 
        { 
            "Miguel Ribeiro Montenegro,MRM,mrm@isep.ipp.pt", 
            "Maria Figueiredo Castro,MFC,mfc@isep.ipp.pt", 
            "Ana Carvalho Cabeça,ACC,acc@isep.ipp.pt", 
            "Bárbara Rosas Silva,BRS,brs@isep.ipp.pt", 
        };
    }
}
