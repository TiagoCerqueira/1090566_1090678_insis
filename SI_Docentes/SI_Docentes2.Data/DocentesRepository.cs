﻿using SI_Docentes2.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI_Docentes2.Data
{
    public class DocentesRepository : IDocentesRepository
    {
        private DocentesContext _ctx;
        public DocentesRepository(DocentesContext ctx)
        {
            _ctx = ctx;
        }

        public IQueryable<Docente> GetAllDocentes()
        {
            return _ctx.Docentes.AsQueryable();
        }

        public Docente GetDocente(int docenteId)
        {
            return _ctx.Docentes.Find(docenteId);
        }

        public bool Insert(Docente docente)
        {
            try
            {
                _ctx.Docentes.Add(docente);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(Docente originalDocente, Docente updatedDocente)
        {
            _ctx.Entry(originalDocente).CurrentValues.SetValues(updatedDocente);
            return true;
        }

        public bool DeleteDocente(int id)
        {
            try
            {
                var entity = _ctx.Docentes.Find(id);
                if (entity != null)
                {
                    _ctx.Docentes.Remove(entity);
                    return true;
                }
            }
            catch
            {
                // TODO Logging
            }

            return false;
        }

        public bool SaveAll()
        {
            return _ctx.SaveChanges() > 0;
        }


    }
}
