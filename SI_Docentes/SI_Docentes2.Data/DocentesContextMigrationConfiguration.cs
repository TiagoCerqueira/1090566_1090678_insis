﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI_Docentes2.Data
{
    class DocentesContextMigrationConfiguration : DbMigrationsConfiguration<DocentesContext>
    {
        public DocentesContextMigrationConfiguration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = true;
 
        }
 
        #if DEBUG
        protected override void Seed(DocentesContext context)
        {
           // new DocentesDataSeeder(context).Seed();
        }
        #endif
    }
}
