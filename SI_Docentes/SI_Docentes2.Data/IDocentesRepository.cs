﻿using SI_Docentes2.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI_Docentes2.Data
{
    public interface IDocentesRepository
    {
        IQueryable<Docente> GetAllDocentes();
        Docente GetDocente(int docenteId);

        bool Insert(Docente docente);
        bool Update(Docente originalDocente, Docente updatedDocente);
        bool DeleteDocente(int id);

        bool SaveAll();
    }
}
