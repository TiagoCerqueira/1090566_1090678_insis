﻿using SI_Docentes2.Data.Entities;
using SI_Docentes2.Data.Mappers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI_Docentes2.Data
{
    public class DocentesContext : DbContext
    {
        public DocentesContext() :
            base("SI_Docentes2Connection")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DocentesContext, DocentesContextMigrationConfiguration>());
        }
 
        public DbSet<Docente> Docentes { get; set; }
 
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new DocenteMapper());

            base.OnModelCreating(modelBuilder);
        }
    }
}
