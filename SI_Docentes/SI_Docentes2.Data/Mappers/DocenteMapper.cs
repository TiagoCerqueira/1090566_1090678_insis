﻿using SI_Docentes2.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SI_Docentes2.Data.Mappers
{
    class DocenteMapper : EntityTypeConfiguration<Docente>
    {
        public DocenteMapper()
        {
            this.ToTable("Docentes");
 
            this.HasKey(c => c.Id);
            this.Property(c => c.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(c => c.Id).IsRequired();
 
            this.Property(c => c.Nome).IsRequired();
            this.Property(c => c.Nome).HasMaxLength(255);

            this.Property(c => c.Sigla).IsRequired();
            this.Property(c => c.Sigla).HasMaxLength(5);

            this.Property(c => c.Email).IsRequired();
            this.Property(c => c.Email).HasMaxLength(70);
        }
    }
}
