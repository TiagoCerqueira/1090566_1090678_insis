/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebServices;

import java.util.LinkedList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

/**
 *
 * @author tiago
 */
@WebService(serviceName = "AlunosAPI")
public class AlunosAPI
{

    @WebMethod(operationName = "create")
    public boolean create(@WebParam(name = "numMec") int numMec, @WebParam(name = "nome") String nome, @WebParam(name = "morada") String morada, @WebParam(name = "email") String email, @WebParam(name = "telefone") int telefone)
    {
        String query = "";
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try
        {
            org.hibernate.Transaction tx = session.beginTransaction();
            query = "INSERT INTO APP.ALUNOS VALUES (" + numMec + ",'" + nome + "','" + morada + "','" + email + "'," + telefone + ")";
            session.createSQLQuery(query).executeUpdate();
            tx.commit();
            //session.close();
            return true;
        } catch (Exception e)
        {
            e.printStackTrace();
        }finally
        {
            //session.close();
            return false;
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "delete")
    public boolean delete(@WebParam(name = "numMec") int numMec)
    {
        String query = "";
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try
        {
            org.hibernate.Transaction tx = session.beginTransaction();
            query = "DELETE FROM APP.ALUNOS WHERE numMec=" + numMec;
            session.createSQLQuery(query).executeUpdate();
            tx.commit();
            //session.close();
            return true;
        } catch (Exception e)
        {
            e.printStackTrace();
        }finally
        {
            //session.close();
            return false;
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "read")
    public List<Alunos> read()
    {
        String query = "";
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        List<Alunos> list = null;
        try
        {
            Alunos al;
            int numMec, telefone;
            String morada, email, nome;
            org.hibernate.Transaction tx = session.beginTransaction();
            query = "SELECT * FROM APP.ALUNOS";
            SQLQuery q = session.createSQLQuery(query);
            q.addEntity(Alunos.class);
            list = q.list();
            tx.commit();
    //        session.close();
            return list;
        } catch (Exception e)
        {
            e.printStackTrace();
        } finally
        {
            //session.close();
            return list;
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "update")
    public boolean update(@WebParam(name = "numMec") int numMec, @WebParam(name = "nome") String nome,@WebParam(name = "morada") String morada,@WebParam(name = "email") String email,@WebParam(name = "telefone") double telefone)
    {
        String query = "";
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try
        {
            org.hibernate.Transaction tx = session.beginTransaction();
            query = "UPDATE APP.ALUNOS SET NOME='" + nome + "',MORADA='" + morada + "',EMAIL='" + email + "',TELEFONE=" + telefone + " WHERE NUMMEC=" + numMec;
            session.createSQLQuery(query).executeUpdate();
            tx.commit();
 //           session.close();
            return true;
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        } finally
        {
            //session.close();
            return false;
        }
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "readById")
    public List<Alunos> readById(@WebParam(name = "numMec") int numMec)
    {
        String query = "";
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        List<Alunos> list = null;
        try
        {
            org.hibernate.Transaction tx = session.beginTransaction();
            query = "SELECT * FROM APP.ALUNOS WHERE NUMMEC="+numMec;
            SQLQuery q = session.createSQLQuery(query);
            q.addEntity(Alunos.class);
            list = q.list();
            tx.commit();
            //session.close();
            return list;
        } catch (Exception e)
        {
            e.printStackTrace();
        }finally
        {
            //session.close();
            return list;
        }
    }

}
